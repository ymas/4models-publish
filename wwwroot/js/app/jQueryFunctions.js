(function( $ ){
    $.fn.myDelete = function() {
        let _this = this;
        let _parentNode = _this.data("parent") || _this.parents("tr");
        let delUrl = _this.prop('nodeName').toLowerCase() === "button" ? (_this.data("url") === undefined ? _this.attr("url") : _this.data("url")) :  _this.attr("href");
        swal({
                title: "تأكيد الحذف؟",
                text: "سوف تقوم بحذف البيانات ! هل تريد الاستمرار؟",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: "موافق",
                cancelButtonText: "الغاء",
            },function()  {
                setTimeout(function () {
                    
                }, 500);   
                $.ajax({
                    url: delUrl,
                    type: 'DELETE',
                    contentType: 'JSON',
                    statusCode: {
                        404: function() {
                            swal("404", "تعذر الوصول للصفحة المطلوبة", "error");
                        }},
                    success : function (res) {
                        //console.log(res);
                        if ( res.success === 1 ) {
                            swal({ title:"تم الحذف", type:'success', text: "تم حذف البيانات بنجاح" , showConfirmButton:false, timer: 1500});
                            console.log(_parentNode);
                            $(_this).parents(_parentNode).hide();
                        } else {
                            swal("خطأ", "حدث خطا اثناء عملية الحذف", "error");
                            console.log(res.error);
                        }
                    }
                }).error(function(x,y,errorText){
                       console.log(errorText);
                        swal("خطأ", errorText, "error");
                    });
            });
        return false;
    };
})( jQuery );
