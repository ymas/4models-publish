var ctx = document.getElementById('goalsByYears');

$.get('/Home/GetStrategicGoalsChart', function (res) {
    let values = [];
    let labels = [];
    res.data.forEach(x => {
        values.push(TruncateFloat(x.achievement,2));
        labels.push(x.year);
    });


    let myChart = new Chart(ctx, {
        type: 'line',
        
        data: {
            labels: labels,
            datasets: [{
                label: document._translate_achievement,
                data: values,
                fill: true,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 0
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    display: true,
                    ticks: {
                        beginAtZero: true,   // minimum value will be 0.
                        max: 100   // minimum value will be 0.
                    }
                }]
            },
            responsive: true,
            title: {
                display: false,
                text: document._translate_strategic_plan_progress
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            }
        }
    });

}, 'json');




