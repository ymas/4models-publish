$(function(){
    $("#selectAllCheckBoxes").click(function () {
       
        if($("#selectAllCheckBoxes").is(":checked")) {
            $("input[name='SelectedStandards']").prop("checked",  true);
            $(this).attr("checked", true);
        } else {
            $("input[name='SelectedStandards']").prop("checked",  false);
            $(this).attr("checked", false);
        }
    })
});

 