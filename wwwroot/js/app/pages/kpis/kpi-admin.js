let KpiAdmin = function () {

    let standardsDropList = $("#StandardId");

    this.init = function(table){
        if($.cookie("StandardId") != null)
        {
            standardsDropList.val($.cookie("StandardId"));
        }else{
            $.cookie("StandardId",standardsDropList.val());
            standardsDropList.val($.cookie("StandardId")).trigger('change');
        }
    };

    standardsDropList.on("change",
        function() {
            let  value = $(this).val();
            $.cookie("StandardId",value);
        }); 
}; // jquery 

function reloadTable(table)
{
    if(table !== undefined)
    {
        table.ajax.reload();
    }
}

let table;

$(document).ready(function() {



    $("#resetForm").click(function() {
        $(this).parents("form").find("input").val('');
        $(this).parents("form").find("select").val('').trigger("change");
    });

    table = $('#kpitable').DataTable({
        'dom': 'rltip',
        "processing": true,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': '<div class="spinner"></div>'
        },
        responsive: false,
        "ordering": false,
        "stateSave": true,
        "serverSide": true,
        "ajax": {
            "url": '/Kpis/AdminDt',
            "type": 'POST',
            "dataType": "json",
            "data": function(d) {
                searchParm(d);
            }
        },
        "columns": [
            {
                "width": "50",
                "className": "center",
                "render": function(x, y, data) {
                    if(data.rate === null) return ` <span class="badge  badge-danger">N/A</span>`;
                    return ` <span class="badge  badge-success">${TruncateFloat(data.rate,2)}%</span>` ; 
                }
            },
            {
                "render": function(x,y,data) {
                    return '<a href="/Kpis/Details/' +
                        data.id +
                        '" >' +
                         data.name +
                        '.</a>';
                }
            },
            {
                "width": "100",
                "render": function(x,y, data)
                {
                    return data.type;
                }
            },
            {
                "width": "100",
                "render": function(x,y, data)
                {
                    return   ` <span class="badge  badge-primary">${data.cycle}${data.method !== '-' ? ' / '+data.method  : ''}</span>`;
                }
            },
            {
                "width": "50",
                "className" : 'center',
                "render": function(x,y, data)
                {
                    return ` <span class="badge  badge-info">${data.direction}</span>` ;
                }
            },
            {
                "data": "lastUpdate",
                "width": "100",
                "className" : "nowrap",
                "render": function(x,y, data)
                {
                    return data.lastUpdate;
                }
            },
            {
                "width": "1",
                "render" : function()
                {
                    return ' ';
                }
            },
            {
                "data": "actions",
                "width": "100",
                "className": "center",
                "render": function(x,y,data) {
                    return dropDownActionBtn(data.id);
                }
            }

        ]
    });


    table.on('draw',
        function(data) {
            HideLoader();
            $("button.edit").click(function() {
                if ($(this).hasAttr("data-url")) {
                    document.location = $(this).data("url");
                }
                return false;
            });

            // $("a.colorbox").click(function() {
            //     $.colorbox({
            //         iframe: true,
            //         href: $(this).attr("href"),
            //         onClose: function(){
            //            
            //         }
            //     });
            //     return false;
            // });

            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            elems.forEach(function(elem) {
                var switchery = new Switchery(elem, { color: '#7c8bc7', size: 'small' });
                elem.onchange = function() {
                    $.post('/Kpis/SetAsPerformanceDepartment',
                        { id: elem.value },
                        function(res) {
                            if (res.success === 1) {
                                $.notify("تم تغيير الحالة بنجاح", "success", { position: "left" });
                            } else {
                                $.notify("حدث خطا اثناء عملية تغيير الحالة", "danger", { position: "left" });
                            }
                        },
                        'json');
                };
            });

        });

    $.notify.defaults({ position: "top center" });


    // $('#doFilter').click(function() {
    //     if (parseInt($("#Year").val()) > 0) {
    //         $("#mYear").html('[ ' + $("#Year").val() + ' ]');
    //     }
    //     ShowLoader();
    //     table.ajax.reload();
    //
    // });


 $('#dtAjaxForm select').change(function() {
     let _this = $(this);
        if (parseInt($("#Year").val()) > 0) {
            $("#mYear").html('[ ' + $("#Year").val() + ' ]');
        }
        table.ajax.reload();
    });

    $('#dtAjaxForm input').keyup(function() {
        let _this = $(this);
        table.ajax.reload();
        return false;
    });


    $('#standardId').select2({
        allowClear: true,
        ajax: {
            url: '/Standards/Select2',
            data: function(params) {
                return {
                    q: params.term,
                    page: params.page || 1
                };
            }
        }
    });


    function searchParm(dd) {
        dd.Code = $("#Code").val();
        dd.StandardId = $("#StandardId").val();
        dd.PillarId = $("#PillarId").val();
        dd.TypeId = $("#TypeId").val();
        dd.CategoryId = $("#CategoryId").val();
        dd.DepartmentId = $("#DepartmentId").val();
        dd.Name = $("#Name").val();
        dd.ResultCycle = $("#ResultCycle").val();
        dd.CalculationMethod = $("#CalculationMethod").val();
        dd.Direction = $("#Direction").val();
        dd.Year = $("#Year").val();
        dd.ResultRank = $("#ResultRank").val();
    }

    $(document).bind('cbox_complete', function(){
        table.ajax.reload();
    });

    $("#StandardId").on("change",
        function() {
            let  value = $(this).val();
            $.cookie("StandardId",value);
            $("#doFilter").trigger("click");
        });

    init();

}); // jquery


/**
 * @return {boolean}
 */
function SubmitKpiResult(form) {
    SubmitForm(form, table);
    return false;
}

function init()
{
    if($.cookie("StandardId") != null)
    {
        $("#StandardId").val($.cookie("StandardId")).trigger("change");
        $("#doFilter").trigger("click");
    }else{
        $.cookie("StandardId",$("#StandardId").val());
    }

}

function dropDownActionBtn(id)
{
    let tr_updateResults = $("#UpdateResults").val();
    let tr_edit = $("#Edit").val();
    let tr_delete = $("#Delete").val();
    let tr_clone = $("#CloneKpi").val();
    let tr_action = $("#Action").val();
    
    return '<div class="dropdown">\n'+
        '  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">'+tr_action+'  \n'+
        '  <span class="caret"></span></button>\n'+
        '  <ul class="dropdown-menu">\n'+
        '    <li><a onclick="openColorBox(this);  return false" class=" text-right" data-toggle="tooltip" title="'+tr_updateResults+'" href="/KpisResults/UpdateResults?kpiId=' + id + '&_year=@(DateTime.Now.Year)" ><i class="fa fa-bar-chart text-dark"></i> '+tr_updateResults+'</a></li>\n'+
        '    <li><a class="text-right" data-toggle="tooltip" target="_blank" title="'+tr_edit+'" href="/Kpis/Edit/' + id + ' " ><i class="fa fa-edit text-info"></i> '+tr_edit+'</a></li>\n'+
        '    <li><a class="text-right" data-toggle="tooltip" title="'+tr_clone+'" href="/Kpis/CloneKpi/' + id + ' " ><i class="fa fa-copy text-warning"></i> '+tr_clone+'</a></li>\n'+
        '    <li><a class="text-right" data-toggle="tooltip" title="'+tr_delete+'" href="/Kpis/Delete/' + id + ' " ><i class="fa fa-remove text-danger"></i> '+tr_delete+'</a></li>\n'+
        '  </ul>\n'+
        '</div>';
}


function openColorBox(elm) {
    $.colorbox({
        iframe: true,
        href: $(elm).attr("href")
    });
    return false;
} 