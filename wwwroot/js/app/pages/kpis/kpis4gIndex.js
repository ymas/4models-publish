let Kpis4gIndex = function () {

    let dataTableObject = {};
    let dataTableElement = $("#kpiDataTable");
    let standardsDropList = $("#StandardsDropList");
    let translate= {
        Delete:'Delete',
        Edit:'Edit',
    };
    
    this.init = function(){
        LoadTranslation();
        if($.cookie("activeStandardId") != null)
        {
            standardsDropList.val($.cookie("activeStandardId"));
        }else{
            $.cookie("activeStandardId",standardsDropList.val());
        }
        drawEnablersTable();
    };

    standardsDropList.on("change",
        function() {
            let  value = $(this).val();
            $.cookie("activeStandardId",value);

            $("#evaluationResultsLink")
                .prop("href", '/Standards/Results/' + value);
            dataTableObject.ajax.url('/Kpis/FourthGenerationKpisDataTable/' + value);
            dataTableObject.ajax.reload();
        }); // onchnage


   

    window.reloadTable = function(a) {
        window.DeleteDataTableRow(a, dataTableObject);
    };
        
    let LoadTranslation = function()
    {
        
        $.post('/api/LoadTranslation',{words:['Delete','Edit','Evaluate','ShowHideColumns','ExportToExcel','Print']}, function(res){
            translate = res ;
        },'json');
    };
    

    let drawEnablersTable = function()
    {
        dataTableObject = dataTableElement.DataTable(
            {
                'dom': "<'row'<'col-md-10'f><'col-md-2'<'pull-left' l>>>tip",
                "deferRender": true,
                'ajax':
                    {
                        url: '/Kpis/FourthGenerationKpisDataTable/'+standardsDropList.val(),
                        dataType: 'json',
                        type: 'get'

                    },
                'columns': [
                    {
                        "width": "5%",
                        "class": "center",
                        "render": function (data, type, row) {
                            return '<input style="width:50px" id="' +
                                row.order.id +
                                '" type="text" value="' +
                                row.order.seq +
                                '" size="3" class="form-control seq center">';
                        }
                    },
                    {
                        "width": "50%",
                        "render": function (data, type, row) {
                            return '<a href="/Kpis/Details/' +
                                row.name.id +
                                '">' +
                                row.name.name +
                                '</a>';
                        }
                    },
                    { "data": "lastUpdate" },
                    { "data": "totalEnablers" },
                    {
                        "data": "rate",
                        "render": function(data) {
                            return '<a target="_blank" href="/Auditing/KpiAuditingDetails/' +
                                data.id +
                                '" >' +
                                data.rate +
                                '</a>';
                        }

                    },
                    {
                        "render": function(x,y,row) {
                            return '';
                            // '<a target="_blank" href="/Kpis/Edit/' +
                            //     row.name.id +
                            //     '" > edit</a>' ;
                        }

                    }
                ],
                "drawCallback": function () {

                    $('input.seq').on("change",
                        function () {
                            //let _this = $(this);
                            ShowLoader();
                            $.post('/Kpis/UpdateOrdering',
                                {"Progress": $(this).val(), "Id": $(this).attr("id")},
                                function (res) {
                                    if ( res.success === 1 ) {
                                        HideLoader();
                                        $.notify("تم تغيير الترتيب بنجاح",
                                            {
                                                globalPosition: "top center",
                                                className: "success"
                                            });
                                        dataTableObject.ajax.reload();
                                    } else {
                                        HideLoader();
                                        $.notify("تعذر تغيير حالة المهمة!!",
                                            {
                                                globalPosition: "top center",
                                                className: "danger"
                                            });
                                    }
                                });
                        });
                }
            });
    };

    

  
    // /**
    //  * @return {bo
    //  olean}
    //  */
    // let CreateNewAttachment = function (id) {
    //     $.colorbox({
    //         href: '/Attachments/Create/' + id,
    //         iframe: false,
    //         width: '50%',
    //         height: '550',
    //         onLoad: function () {
    //             $("#colorbox").removeAttr("tabindex");
    //         },
    //         onComplete: function () {
    //             document.LoadGlobalConfigurations();
    //         }
    //     });
    //     return false;
    // };

    // return {
    //     init: function () {
    //         drawEnablersTable();
    //     }
    //};
}; // jquery



