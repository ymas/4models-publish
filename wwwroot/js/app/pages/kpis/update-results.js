var UpdateResults = function () {
    /**
     * @return {boolean}
     */
    let UpdateResult = function (id) {
        $.colorbox({
            href: '/KpisResults/UpdateResults/' + id,
            iframe: false,
            innerWidth: "50%",
            innHeight: 400,
            onComplete: function () {
                $.colorbox.resize();
                $("#TargetsNotEqualAnnualTarget").addClass("hide");
            }
        });
        return false;
    };

   

    

    document.DeleteResult = function (id) {
        if ( confirm('تأكيد عملية الحذف - Confirm deletion !') ) {
            $.ajax({
                url: '/Kpis/DeleteResult/' + id,
                dataType: 'json',
                type: 'DELETE',
                success: function (res) {
                    if ( res.success === 1 ) {
                        $.notify('تم حذف النتيجة بنجاح', "success");
                        document.location = document.location;
                    } else {
                        alert(res.error);
                    }
                }
            });
        }
    };

    document.SplitTargetResult = function (elm) {
        let _this = $(elm);
        $("input.quarterlyTarget").removeClass("error");
        let method = _this.data("method").valueOf().toLowerCase();
        let cycle = _this.data("cycle").valueOf().toLowerCase();
        let targetValue = parseFloat($("input#Target").val());
        swal({
            title: $("#SplitAnnualTarget").val(),
            text: $("#SplitChangeTargetMessage").val(),
            dangerMode: true,
            icon: 'warning',
            buttons: {
                cancel: {
                    text: $("#No").val(),
                    value: false,
                    visible: true,
                    className: "",
                    closeModal: true,
                },
                confirm: {
                    text: $("#Yes").val(),
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                }
            }
        }).then((x) => {
            if ( x ) {
                $("input.quarterlyTarget").removeClass("error");
                $("#doSave").attr("disabled", false);
                if ( !$.isNumeric(targetValue) ) {
                    $("input[name ^='Target']").val('');
                    return false;
                }
                if ( targetValue === 0 ) {
                    $("input[name ^='Target']").val(0);
                    return false;
                }
                if ( cycle !== 'annually' ) {
                    let cyclesNumber = cycle === 'quarterly' ? 4 : 2;
                    if ( method === "split" && targetValue > 0 ) {
                        let v = (targetValue / parseInt(cyclesNumber));
                        $(_this).parents(".panel").find("#Target1").val(v);
                        $(_this).parents(".panel").find("#Target2").val(v);
                        if ( parseInt(cyclesNumber) > 2 ) {
                            $(_this).parents(".panel").find("#Target3").val(v);
                            $(_this).parents(".panel").find("#Target4").val(v);
                        }
                    } else {
                        $(_this).parents(".panel").find("#Target1").val(targetValue);

                        $(_this).parents(".panel").find("#Target2").val(targetValue);
                        if ( parseInt(cyclesNumber) > 2 ) {
                            $(_this).parents(".panel").find("#Target3").val(targetValue);

                            $(_this).parents(".panel").find("#Target4").val(targetValue);
                        }
                    }
                }
            }
        }); // end swal
    };

    document.changeMainTarget = function (newValue) {
        let targetValue = parseFloat(newValue);
        let cycle = $("input[name='ResultCycle']:checked").val().toLowerCase();
        let method = $("input[name='CalculationMethod']:checked").val().toLowerCase();
        if ( targetValue > -1 ) {

            if ( cycle !== 'annually' ) {
                let cyclesNumber = cycle === 'quarterly' ? 4 : 2;
                if ( method === "split" && targetValue > 0 ) {
                    let v = (targetValue / parseInt(cyclesNumber));
                    $('.panel').find("#Target1").val(v);
                    $('.panel').find("#Target2").val(v);
                    if ( parseInt(cyclesNumber) > 2 ) {
                        $(".panel").find("#Target3").val(v);
                        $(".panel").find("#Target4").val(v);
                    }
                } else {
                    $(".panel").find("#Target1").val(targetValue);
                    $(".panel").find("#Target2").val(targetValue);
                    if ( parseInt(cyclesNumber) > 2 ) {
                        $(".panel").find("#Target3").val(targetValue);

                        $(".panel").find("#Target4").val(targetValue);
                    }
                }
            }
        }
    };

    document.onChangeCycle = function (elem) {
        $("#doSave").attr("disabled", true);
        document.location = '/KpisResults/UpdateResults?kpiId=' + $(elem).attr("kpiId") + '&_year=' +
            $('#Year').val() +
            '&cycle=' +
            $(elem).val() +
            '&id=' + $(elem).data("id");
    };

    let init = function (id) {
        $("#doSave").attr("disabled", false);
        $("#Year").change(function () {
            document.location.href = '/KpisResults/UpdateResults?kpiId=' + id + '&_year=' + $(this).val();
        });
        UpdateResult(id);
    }
};

UpdateResults.prototype.ValidateForm = function (obj) {

    $("#updateForm").validate({
        rules: {
            'Target': {required: true, number: true},
            'Target1': {
                required: function () {
                    return obj.cycle === 'quarterly' || obj.cycle === 'monthly' || obj.cycle === 'semiAnnually';
                },
                number: function () {
                    return obj.cycle === 'quarterly' || obj.cycle === 'monthly' || obj.cycle === 'semiAnnually';
                }
            },
            'Target2': {
                required: function () {
                    return obj.cycle === 'quarterly' || obj.cycle === 'monthly' || obj.cycle === 'semiAnnually';
                },
                number: function () {
                    return obj.cycle === 'quarterly' || obj.cycle === 'monthly' || obj.cycle === 'semiAnnually';
                }
            },
            'Target3': {
                required: function () {
                    return obj.cycle === 'quarterly' || obj.cycle === 'monthly';
                },
                number: function () {
                    return obj.cycle === 'quarterly' || obj.cycle === 'monthly';
                }
            },
            'Target4': {
                required: function () {
                    return obj.cycle === 'quarterly' || obj.cycle === 'monthly';
                },
                number: function () {
                    return obj.cycle === 'quarterly' || obj.cycle === 'monthly';
                }
            }

        }
    });
};

UpdateResults.prototype.ShowBaseZero = function (DecreaseIsBest) {
    if ( DecreaseIsBest === true ) {
        $("#Target").change(function () {
            if ( $(this).val() === 0 ) {
                $("#zeroBaseTr").removeClass('hide')
            } else {
                $("#zeroBaseTr").addClass('hide')
            }
        });
    }
};

document.ValidateTargets = function (elm , decreaseIsBest) {
    let method = $("input[name='CalculationMethod']:checked").val();
    let cycle = $("input[name='ResultCycle']:checked").val();
    let target = parseFloat($("input#Target").val());
    let totalAll = 0;
    let t1 = parseFloat($("input[name='Target1']").val());
    let t2 = parseFloat($("input[name='Target2']").val());
    let t3 = parseFloat($("input[name='Target3']").val());
    let t4 = parseFloat($("input[name='Target4']").val());
    $('input.quarterlyTarget').each(function () {
        totalAll += parseFloat($(this).val());
    });
    let value = parseFloat($(elm).val());
    if ( method === 'split' && totalAll !== target && decreaseIsBest !== 'True') {
        $("#TargetsNotEqualAnnualTarget").removeClass("hide");
        $("input.quarterlyTarget").addClass("error");
        $("#doSave").attr("disabled", true);
    }  else
    if ( method === 'repeat' && (value !== target ||  (t1 !== target ||t2 !== target ||t3 !== target ||t4 !== target)) ) {
        $("#SemiAnnualTargetsErrors").removeClass("hide");
        $("input.quarterlyTarget").addClass("error");
        $("#doSave").attr("disabled", true);
    } else
    if ( method === 'last') {
        let valid = true;
        if(cycle === "semiannually" && (t2 !== target || t1 >  target))
        {
         valid=false
        }
        if(cycle === "quarterly" && (t4 !== target || (t1 >  target || t2 >  target || t3 >  target )))
        {
            valid=false
        }
        if(!valid) {
            $("#TargetsNotEqualAnnualTarget").removeClass("hide");
            $("input.quarterlyTarget").addClass("error");
            $("#doSave").attr("disabled", true);
        } else {
            $("#TargetsNotEqualAnnualTarget").addClass("hide");
            $("input.quarterlyTarget").removeClass("error");
            $("#doSave").attr("disabled", false);
        }
       
    } 
    else {
        $("#TargetsNotEqualAnnualTarget").addClass("hide");
        $("#SemiAnnualTargetsErrors").addClass("hide");
        $("input.quarterlyTarget").removeClass("error");
        $("#doSave").attr("disabled", false);
    }
};

$("input").focus(function(){
    if($(this).val() === "-") $(this).val('');
});

$("input#Target").on('change', function () {
    if(isNaN($(this).val())){
        $("input.quarterlyTarget").attr("readOnly", true).addClass("disabled");
        $("input[name^='Value']").attr("readOnly", true).addClass("disabled");
    } else{
        $("input.quarterlyTarget").attr("readOnly", false).removeClass("disabled");
        $("input[name^='Value']").attr("readOnly", false).removeClass("disabled");
    }
});
$("input#Target").trigger('change');

$("input[name='CalculationMethod']").click(function(){
    let _this = $(this);
    document.querySelector('#autoCalcTargets').setAttribute('data-method',_this.val());
    if($("input[name='ResultCycle']").val() != 'annually')
    {
        _this.val() === 'last' ?  $('#autoCalcTargets').attr('disabled',true) : $('#autoCalcTargets').attr('disabled',false)
    } else {
        _this.attr("disabled", true)
    }
});