
//TODO: make upload file in once file and validate the file size and file type in client size
$(function () {
    $("#AddNewAttachment").click(function() {
        var url = $(this).attr("href");
        $.colorbox({
            href: url,
            iframe: false,
            width: '50%',
            height: '600',
            onLoad: function() {
                $("#colorbox").removeAttr("tabindex");
            },
            onComplete: function() {
                document.LoadGlobalConfigurations();
                $("#addNewAttachmentForm").validate({
                    
                });
            }
        });
        return false;
    }); 
});

FileValidation = () => {
    const fi = document.getElementById('UpFile');
    // Check if any file is selected. 
    if (fi.files.length > 0) {
        for (let i = 0; i <= fi.files.length - 1; i++) {

            const fsize = fi.files.item(i).size;
            const file = Math.round((fsize / 1024));
            // The size of the file. 
            if (file >= 4096) {
                alert(
                    "File too Big, please select a file less than 4mb");
            } else if (file < 2048) {
                alert(
                    "File too small, please select a file greater than 2mb");
            } else {
                document.getElementById('size').innerHTML = '<b>'
                    + file + '</b> KB';
            }
        }
    }
};